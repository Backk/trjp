import React, {useContext, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import UserContextProvider from './src/context/UserContext';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import routes from './src/routes';
import {HomeRoute} from './src/routes';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {AuthContext} from './src/context/AuthContext';
import Login from './src/View/Login/login';
import Register from './src/View/Register/register';
import User from './src/View/User';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const Home = () => {
  const {authState} = useContext(AuthContext);

  return (
    <Tab.Navigator
      screenOptions={{
        tabBarActiveTintColor: '#584CF4',
        headerShown: false,
        tabBarStyle: {
          height: 60,
          paddingBottom: 5,
          paddingTop: 5,
        },
      }}
      initialRouteName="Home">
      {HomeRoute.map((e, index) => {
        return (
          <Tab.Screen
            key={index}
            options={{
              tabBarIcon: ({color, size}) => (
                <FontAwesome5 color={color} name={e.icon} size={size} />
              ),
            }}
            name={e.name}
            component={e.component}
          />
        );
      })}
      {authState.isAuthenticated === true ? (
        <Tab.Screen
          options={{
            tabBarIcon: ({color, size}) => (
              <FontAwesome5 color={color} name="user" size={size} />
            ),
          }}
          name={'User'}
          component={User}
        />
      ) : (
        <Tab.Screen
          options={{
            tabBarIcon: ({color, size}) => (
              <FontAwesome5 color={color} name="user" size={size} />
            ),
            title: 'User',
            tabBarStyle: {
              display: 'none',
            },
          }}
          name={'Login'}
          component={Login}
        />
      )}
    </Tab.Navigator>
  );
};

function App() {
  return (
    <UserContextProvider>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Homes">
          <Stack.Screen
            name="Homes"
            component={Home}
            options={{headerShown: false}}
          />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Register" component={Register} />

          {routes.map((e, index) => {
            return (
              <Stack.Screen
                options={({route}) => ({
                  title: route.params?.name,
                  headerTitleAlign: route.params?.headerStyle,
                  headerShown: e.headerShown,
                })}
                key={e.name}
                name={e.name}
                component={e.component}
              />
            );
          })}
        </Stack.Navigator>
      </NavigationContainer>
    </UserContextProvider>
  );
}

export default App;
