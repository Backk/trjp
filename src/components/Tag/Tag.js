import {View, StyleSheet, Text} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import CustomText from '../CustomText';

function Tag({tagName, backgroundColor, icon, ...props}) {
  return (
    <View style={{...styles.container, backgroundColor, ...props?.style}}>
      <View style={styles.wrapper}>
        <FontAwesome5 name={icon} size={20} color="white" style={styles.logo} />
        <CustomText content={tagName} styles={styles.tagName} />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    width: 97,
    height: 40,
    borderRadius: 20,
    flexDirection: 'row',
    marginLeft: 12,
    marginRight: 12,
    alignItems: 'center',
    justifyContent: 'center',
  },
  wrapper: {
    display: 'flex',
    width: '85%',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  tagName: {
    color: 'white',
    fontSize: 14,
    fontWeight: '400',
    marginLeft: 4,
  },
  logo: {
    marginRight: 4,
  },
});

export default Tag;
