import React from 'react';
import {Text, StyleSheet} from 'react-native';

function CustomText({content, styles, ...props}) {
  return (
    <Text {...props} style={{..._styles.style, ...styles}}>
      {content}
    </Text>
  );
}

const _styles = StyleSheet.create({
  style: {
    fontFamily: 'Inter-Regular',
    color: 'black',
  },
});

export default CustomText;
