import * as Progress from 'react-native-progress';
import {View} from 'react-native';

function Loading(props) {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Progress.Circle
        color={props.color || 'white'}
        borderWidth={2}
        size={30}
        indeterminate={true}
      />
    </View>
  );
}

export default Loading;
