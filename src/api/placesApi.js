import axiosClient from './axiosClient';

const placesApi = {
  getPlaces(data) {
    const url = '/place/get-place';
    return axiosClient.post(url, data);
  },
  searchPlace(searchString) {
    const url = '/place';
    return axiosClient.post(url, {search: searchString});
  },
  getPlaceById(id) {
    const url = '/place/get-place-by-id';
    return axiosClient.post(url, {id});
  },
  buyTicket(data) {
    const url = '/place/buy-ticket';
    return axiosClient.post(url, data);
  },
};

export default placesApi;
