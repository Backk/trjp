import dayjs from 'dayjs';
const checkValidTime = () => {
  const currentHour = dayjs(Date.now()).format('H');
  const currentFullHour = dayjs(Date.now()).format('HH:mm');
  const today = dayjs(Date.now()).format('YYYY-MM-DD');
  const startDateTime = dayjs(`${today} 08:00`);
  const currentDateTime = dayjs(`${today} ${currentFullHour}`);
  const diffHour = currentDateTime.diff(startDateTime, 'hour', true);

  if (diffHour - 9 > 0 || currentHour < 8) return false;
  return true;
};

export default checkValidTime;
