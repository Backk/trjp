import CalendarIcon from './Calendar.png';
import LocationIcon from './Location.png';
import AddImageIcon from './addImageIcon.png';
import MapPointIcon from './mapPoint.png';

export default {CalendarIcon, LocationIcon, AddImageIcon, MapPointIcon};
