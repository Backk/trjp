import Home from '../../src/View/Home/home';
import Blog from '../View/Blog/blog';
import EventDetail from '../View/Event/EventDetail';
import CreateEvent from '../View/Event/CreateEvent';
import GuideAreaDetail from '../View/Guide/GuideAreaDetail';
import Guide from '../View/Guide/Guide';
import Search from '../View/Search/search';
import Map from '../View/Search/map';

const routes = [
  {name: 'CreateEvent', component: CreateEvent, title: 'Tạo sự kiện'},
  {name: 'EventDetail', component: EventDetail, headerShown: false},
  {name: 'Search', component: Search, icon: 'search', title: 'Tìm kiếm'},
  {name: 'Map', component: Map, icon: 'search'},
  {name: 'GuideAreaDetail', component: GuideAreaDetail, headerShown: false},
];

const HomeRoute = [
  {name: 'Home', component: Home, icon: 'home'},
  {name: 'Favorite', component: Blog, icon: 'heart'},
  {name: 'Blog', component: Blog, icon: 'pen-square'},
  {name: 'Guide', component: Guide, icon: 'book'},
];

export default routes;
export {HomeRoute};
