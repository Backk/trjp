import React from 'react';
import {createContext} from 'react';
import axiosClient from '../api/axiosClient';

export const UserContext = createContext(null);

const UserContextProvider = ({children}) => {
  const value = {};
  return <UserContext.Provider value={value}>{children}</UserContext.Provider>;
};

export default UserContextProvider;
