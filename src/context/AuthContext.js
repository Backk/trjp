import React from 'react';
import {createContext, useReducer, useEffect, useRef} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {authReducer} from '../reducer/authReducer';
import authApi from '../api/authApi';

export const AuthContext = createContext(null);

const AuthContextProvider = ({children}) => {
  const [authState, dispatch] = useReducer(authReducer, {
    authLoading: true,
    isAuthenticated: false,
    user: null,
  });

  useEffect(() => {
    loadUser();
  }, []);

  const register = async userForm => {
    try {
      const res = await authApi.register(userForm);
      await AsyncStorage.setItem('user', JSON.stringify(res.data.user));
      dispatch({
        type: 'SET_AUTH',
        payload: {isAuthenticated: true, user: res.data.user},
      });
      return res.data;
    } catch (err) {
      console.log(err);
    }
  };
  const login = async userForm => {
    try {
      const res = await authApi.login(userForm);
      await AsyncStorage.setItem('user', JSON.stringify(res.data.user));
      dispatch({
        type: 'SET_AUTH',
        payload: {isAuthenticated: true, user: res.data.user},
      });
      return res.data;
    } catch (err) {
      console.log(err);
    }
  };

  const logout = async () => {
    try {
      await AsyncStorage.removeItem('user');

      await loadUser();
      dispatch({
        type: 'SET_AUTH',
        payload: {isAuthenticated: false, user: null},
      });
    } catch (err) {
      console.log(err);
    }
  };

  //authenticate user
  const loadUser = async () => {
    try {
      const user = await AsyncStorage.getItem('user');

      if (user) {
        console.log('LOAD USER SUCCESS!!');
        dispatch({
          type: 'SET_AUTH',
          payload: {isAuthenticated: true, user: JSON.parse(user)},
        });
      } else {
        console.log('NO USER FOUND!!');
      }
    } catch (err) {
      console.log(err);
    }
  };

  const value = {
    login,
    register,
    authState,
    logout,
    loadUser,
  };
  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export default AuthContextProvider;
