
export const Users = [
  {
    id: 1,
    user: 'Lảo Thẩn Y Tý.',
    caption: 'Trekking thiên đường mây và hoa chi pâu Lảo Thẩn',
    profile_image:
      'https://static2.yan.vn/YanNews/2167221/202003/wibu-la-gi-tai-sao-wibu-bi-ki-thi-4bfcdcd0.png',
    likes: '7,2k',
    image:
      'https://dulichchat.com/wp-content/uploads/2022/10/san-may-lao-than-hoa-chi-pau-dulichchat-25-780x405.jpg',
    comment: '2,1k',
    share: '1k',
  },
  {
    id: 2,
    user: 'Lê Đức',
    caption:
      'Du lịch rừng, núi: Thay vì lưu trú tại các khách sạn thông thường thì thiên đường nghỉ dưỡng xanh theo mô hình Ecolodge tại Sapa đang cực kì thu hút khách du lịch. Vốn được bao bọc bởi môi trường thiên nhiên, núi rừng; ưu tiên sử dụng vật liệu tự nhiên được làm thủ công nhưng vẫn đảm bảo sự tinh tế, sang trọng; trang trí nội, ngoại thất lồng ghép các vật dụng biểu trưng văn hóa địa phương… ',
    profile_image:
      'https://mcdn.coolmate.me/image/May2022/wibu-la-gi-weeaboo-la-gi-su-khac-nhau-giua-wibu-va-otaku_930.jpg',
    likes: '6k',
    image:
      'https://h3jd9zjnmsobj.vcdn.cloud/public/mytravelmap/images/2019/6/11/anhchau8260/fc3f5c67326e62c253cff55be6c03ffd.jpeg',
    comment: '2,1k',
    share: '1k',
  },
  {
    id: 3,
    user: 'Hiếu Non',
    caption: 'Du lịch biển đảo:',
    profile_image:
      'https://cdn.voh.com.vn/voh/thumbnail/2022/07/25/wibu-la-gi-1.jpg',
    likes: '6,1k',
    image:
      'https://h3jd9zjnmsobj.vcdn.cloud/public/mytravelmap/images/2019/6/11/anhchau8260/thumbnail-70/c17ecc7c78952e68f314b3ea61264a94.jpeg',
    comment: '2,1k',
    share: '1k',
  },
];