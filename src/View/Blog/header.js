import React from 'react';
import {View} from 'react-native';
import {StyleSheet, TouchableOpacity, Image} from 'react-native';

const Header = ({setModalVisible, refRBSheet}) => {
  return (
    <View>
      <View style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            marginLeft: 20,
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <Image
            source={require('../../assets/images/logo.png')}
            style={{width: 45, height: 45}}
          />
          <View style={{flexDirection: 'row', marginRight: 20}}>
            <TouchableOpacity
              onPress={() => {
                refRBSheet.current.open();
                setModalVisible(true);
              }}>
              <Image
                source={require('../../assets/images/iconAdd.png')}
                style={{width: 25, height: 25, marginRight: 10}}></Image>
            </TouchableOpacity>
            <Image
              source={require('../../assets/images/iconSearch.png')}
              style={{width: 25, height: 25}}></Image>
          </View>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    height: 50,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderColor: '#4A43EC',
    // marginBottom: 25,
  },
});
export default Header;
