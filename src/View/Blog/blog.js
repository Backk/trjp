import React, {useState, useContext, useRef} from 'react';
import {SafeAreaView, StyleSheet, ScrollView} from 'react-native';
import Header from './header';
import Post from './post';
import {Users} from './user';
import CreatePost from './createPost';

function Blog() {
  const refRBSheet = useRef();
  const [modalVisible, setModalVisible] = useState(false);

  return (
    <SafeAreaView style={styles.container}>
      <Header setModalVisible={setModalVisible} refRBSheet={refRBSheet} />
      <ScrollView>
        <CreatePost
          refRBSheet={refRBSheet}
          modalVisible={modalVisible}
          setModalVisible={setModalVisible}
        />
        {Users.map((post, index) => (
          <Post post={post} key={post.id} />
        ))}
      </ScrollView>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
export default Blog;
