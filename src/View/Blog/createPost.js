import React from 'react';
import {TextInput, TouchableOpacity, View} from 'react-native';
import {Text, StyleSheet, Modal, Image} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import RBSheet from 'react-native-raw-bottom-sheet';

const listposts = [
  {
    name: 'Ảnh/video',
    nameImage: 'image',
  },
  {
    name: 'Cảm xúc/Hoạt động',
    nameImage: 'smile',
  },
  {
    name: 'Camera',
    nameImage: 'camera',
  },
  {
    name: 'Gắn thẻ sự kiện',
    nameImage: 'calendar-alt',
  },
];

const CreatePost = ({modalVisible, refRBSheet, setModalVisible}) => {
  return (
    <View style={{backgroundColor: '#fff'}}>
      <Modals
        modalVisible={modalVisible}
        refRBSheet={refRBSheet}
        setModalVisible={setModalVisible}
      />
      <BottomSheet refRBSheet={refRBSheet} />
      <ViewCreatePost
        refRBSheet={refRBSheet}
        setModalVisible={setModalVisible}
      />
    </View>
  );
};
const ViewCreatePost = ({setModalVisible, refRBSheet}) => {
  return (
    <TouchableOpacity
      onPress={() => {
        setModalVisible(true);
        refRBSheet.current.open();
      }}>
      <View style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              marginLeft: 10,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Image
              source={{
                uri: 'https://static2.yan.vn/YanNews/2167221/202003/wibu-la-gi-tai-sao-wibu-bi-ki-thi-4bfcdcd0.png',
              }}
              style={styles.user}
            />
            <Text style={{marginLeft: 8, fontSize: 16}}>Bạn đang nghĩ gì?</Text>
          </View>
          <FontAwesome5
            name="image"
            size={25}
            style={{color: '#4A43EC', marginRight: 22}}></FontAwesome5>
        </View>
      </View>
    </TouchableOpacity>
  );
};
const Modals = ({modalVisible, refRBSheet, setModalVisible}) => {
  return (
    <Modal
      animationType="slide"
      transparent={false}
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible(!modalVisible);
      }}>
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            backgroundColor: '#999',
            height: 50,
            alignItems: 'center',
          }}>
          <TouchableOpacity onPress={() => setModalVisible(false)}>
            <Image
              source={{
                uri: 'https://img.icons8.com/sf-ultralight/25/undefined/delete-sign.png',
              }}
              style={{width: 25, height: 25, marginLeft: 10}}></Image>
          </TouchableOpacity>
          <Text style={{fontSize: 16}}>Tạo bài viết</Text>
          <TouchableOpacity>
            <Text
              style={{
                width: 52,
                marginRight: 10,
                borderWidth: 1,
                padding: 5,
                textAlign: 'center',
                borderRadius: 5,
                backgroundColor: '#babdcf',
              }}>
              Đăng
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            marginTop: 10,
            marginLeft: 10,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image
              source={{
                uri: 'https://static2.yan.vn/YanNews/2167221/202003/wibu-la-gi-tai-sao-wibu-bi-ki-thi-4bfcdcd0.png',
              }}
              style={styles.user}
            />
            <Text style={{marginLeft: 10}}>Lê Đức</Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              refRBSheet.current.open();
            }}>
            <Image
              source={{
                uri: 'https://img.icons8.com/material-outlined/24/null/connection-status-off.png',
              }}
              style={{height: 25, width: 25, marginRight: 10}}></Image>
          </TouchableOpacity>
        </View>
        <TextInput
          style={{marginLeft: 8, fontSize: 16, width: '99%'}}
          placeholder="Bạn đang nghĩ gì?"
          multiline={true}></TextInput>
      </View>
    </Modal>
  );
};
const takePhotoFromCamera = () => {
  console.log('hi');
};
const BottomSheet = ({refRBSheet}) => {
  return (
    <RBSheet
      ref={refRBSheet}
      closeOnDragDown={true}
      closeOnPressMask={true}
      customStyles={{
        wrapper: {
          backgroundColor: 'transparent',
        },
        container: {
          height: '35%',
        },
        draggableIcon: {
          backgroundColor: '#4A43EC',
        },
      }}>
      <View>
        <TouchableOpacity onPress={() => takePhotoFromCamera()}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              margin: 5,
            }}>
            <FontAwesome5
              name={listposts[0].nameImage}
              size={25}
              style={{
                color: '#4A43EC',
                marginRight: 22,
                margin: 10,
              }}></FontAwesome5>
            <Text>{listposts[0].name}</Text>
          </View>
        </TouchableOpacity>
        {listposts.map((listpost, index) => {
          return (
            <View key={listpost.name}>
              <TouchableOpacity>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    margin: 5,
                  }}>
                  <FontAwesome5
                    name={listpost.nameImage}
                    size={25}
                    style={{
                      color: '#4A43EC',
                      marginRight: 22,
                      margin: 10,
                    }}></FontAwesome5>
                  <Text>{listpost.name}</Text>
                </View>
              </TouchableOpacity>
            </View>
          );
        })}
      </View>
    </RBSheet>
  );
};
const styles = StyleSheet.create({
  container: {
    height: 70,
    marginTop: 15,
  },
  user: {
    width: 40,
    height: 40,
    borderRadius: 50,
  },
});
export default CreatePost;
