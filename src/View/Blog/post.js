import React, {useState, useEffect, useCallback} from 'react';
import {View} from 'react-native';
import {Text, StyleSheet, TouchableOpacity, Image} from 'react-native';

const postIcon = [
  {
    name: 'like',
    image: 'https://cdn-icons-png.flaticon.com/512/929/929769.png',
  },
  {
    name: 'dislike',
    image: 'https://cdn-icons-png.flaticon.com/512/929/929750.png',
  },
  {
    name: 'comment',
    image: 'https://img.icons8.com/ios/50/000000/comments.png',
  },
  {
    name: 'share',
    image: 'https://cdn-icons-png.flaticon.com/512/4855/4855052.png',
  },
  {
    name: 'save',
    image:
      'https://img.icons8.com/external-bearicons-detailed-outline-bearicons/64/000000/external-Save-social-media-bearicons-detailed-outline-bearicons.png',
  },
];

const Post = ({post}) => {
  return (
    <View style={styles.container}>
      <PostHeader post={post} />
      <Caption post={post} />
      <PostImage post={post} />
      <View style={{marginHorizontal: 15, marginTop: 10}}>
        <PostFooter post={post} />
      </View>
    </View>
  );
};
const PostHeader = ({post}) => (
  <View
    style={{
      flexDirection: 'row',
      justifyContent: 'space-between',
      padding: 8,
    }}>
    <View style={{flexDirection: 'row', alignItems: 'center'}}>
      <Image source={{uri: post.profile_image}} style={styles.profile} />
      <Text style={{marginLeft: 5, fontWeight: '700'}}>{post.user}</Text>
    </View>
    <Text style={{fontWeight: '900', marginRight: 10}}>...</Text>
  </View>
);
const PostImage = ({post}) => (
  <View style={{width: '100%', height: 200, padding: 2}}>
    <Image
      source={{uri: post.image}}
      style={{height: '100%', resizeMode: 'cover'}}
    />
  </View>
);
const Caption = ({post}) => {
  const [showMoreButton, setShowMoreButton] = useState(false);
  const [textShown, setTextShown] = useState(false);
  const [numLines, setNumLines] = useState(undefined);

  const toggleTextShown = () => {
    setTextShown(!textShown);
  };

  useEffect(() => {
    setNumLines(textShown ? undefined : 2);
  }, [textShown]);

  const onTextLayout = useCallback(
    e => {
      if (e.nativeEvent.lines.length > 2 && !textShown) {
        setShowMoreButton(true);
        setNumLines(2);
      }
    },
    [textShown],
  );

  return (
    <>
      <Text
        onTextLayout={onTextLayout}
        numberOfLines={numLines}
        style={styles.caption}>
        {post.caption}
      </Text>

      {showMoreButton ? (
        <Text onPress={toggleTextShown} style={{color: '#333', marginLeft: 10}}>
          {textShown ? 'Rút gọn' : 'Xem thêm'}
        </Text>
      ) : null}
    </>
  );
};
const PostFooter = ({post}) => (
  <View
    style={{
      flexDirection: 'row',
      marginBottom: 10,
      justifyContent: 'space-between',
      alignItems: 'center',
    }}>
    <View style={{flexDirection: 'row'}}>
      <Icon imgStyle={styles.footerIcon} imgUrl={postIcon[0].image} />
      <Text
        style={{
          fontWeight: '600',
          marginTop: 2,
          marginLeft: -4,
          marginRight: 10,
        }}>
        {post.likes}
      </Text>
      <Icon imgStyle={styles.footerIcon} imgUrl={postIcon[1].image} />
    </View>
    <View style={{flexDirection: 'row', marginTop: 2}}>
      <Icon imgStyle={styles.footerIcon} imgUrl={postIcon[2].image} />
      <Text style={{fontWeight: '600', marginLeft: -2, marginRight: 10}}>
        {post.comment}
      </Text>
    </View>
    <View style={{flexDirection: 'row'}}>
      <Icon imgStyle={styles.footerIcon} imgUrl={postIcon[3].image} />
      <Text
        style={{
          fontWeight: '600',
          marginTop: 2,
          marginLeft: -2,
          marginRight: 10,
        }}>
        {post.share}
      </Text>
    </View>
    <Icon imgStyle={styles.footerIcon} imgUrl={postIcon[4].image} />
  </View>
);
const Icon = ({imgStyle, imgUrl}) => (
  <TouchableOpacity>
    <Image style={imgStyle} source={{uri: imgUrl}} />
  </TouchableOpacity>
);
const Like = ({post}) => <Text style={{fontWeight: '600'}}>{post.likes}</Text>;
const styles = StyleSheet.create({
  container: {
    margin: 3,
    // borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 0.5,
    elevation: 2,
    backgroundColor: 'white',
  },
  profile: {
    width: 35,
    height: 35,
    borderRadius: 50,
    borderWidth: 1.6,
    borderColor: '#ff8501',
  },
  footerIcon: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  leftFooterIcon: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  caption: {
    marginLeft: 10,
    marginRight: 10,
    textAlign: 'justify',
  },
});
export default Post;
