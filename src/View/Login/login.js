import React, {useState} from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  Switch,
  TouchableOpacity,
  Alert,
  Keyboard,
  Image,
} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import CustomText from '../../components/CustomText';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {useContext} from 'react';
import {AuthContext} from '../../context/AuthContext';
import Loading from '../../components/Loading/Loading';
import images from '../../assets/images/images';

export default Login = ({navigation}) => {
  const [fetching, setFetching] = useState(false);
  const [toggle, setToggle] = useState(false);
  const [Unseenpass, setUnseenpass] = useState(true);
  const {login, authState} = useContext(AuthContext);

  const handleForgotPassword = () => {
    console.log('ForgotPass');
  };
  const handleSignIn = async data => {
    Keyboard.dismiss();
    if (!data.password || !data.phone) {
      Alert.alert('Bạn chưa nhập đủ thông tin');
      return;
    }

    try {
      setFetching(true);
      const res = await login(data);
      if (res.success === true) {
        setFetching(false);
        navigation.navigate('Homes');
      } else {
        console.log(res.message);
        setFetching(false);
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Sai thông tin đăng nhập!!');
      setFetching(false);
    }
  };
  const GoogleLog = () => {
    console.log('Google');
  };
  const FacebookLog = () => {
    console.log('Facebook');
  };
  const SignUp = () => {
    console.log('Sign Up');
  };
  const Remember = () => {
    setToggle(prev => !prev);
    console.log({toggle});
  };

  const LogInSchema = Yup.object().shape({
    phone: Yup.string().matches(
      /(84|0[3|5|7|8|9])+([0-9]{8})\b/g,
      'Invalid Phone',
    ),
    password: Yup.string().min(8, 'Too short!!'),
    // .matches(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/, 'Invalid Password'),
  });

  return (
    <View style={styles.container}>
      <FontAwesome5 name="home" size={70} style={styles.Logo} />
      <CustomText content={'Trjp'} styles={styles.AppName} />
      <CustomText content={'Sign In'} styles={styles.SignIn} />
      <Formik
        validationSchema={LogInSchema}
        initialValues={{phone: '', password: ''}}
        onSubmit={values => handleSignIn(values)}>
        {({handleChange, handleSubmit, handleBlur, errors, values}) => (
          <View>
            <CustomText
              content={errors.phone ?? errors.phone}
              styles={styles.InputInfor}
            />
            <View style={styles.InputWrapper}>
              <FontAwesome5 name="phone" size={20} style={styles.IconInput} />
              <TextInput
                name="phone"
                placeholder="Phone"
                style={styles.textInput}
                onChangeText={handleChange('phone')}
                onBlur={handleBlur('phone')}
                value={values.phone}
              />
            </View>
            <CustomText
              content={errors.password ?? errors.password}
              styles={styles.InputInfor}
            />
            <View style={styles.InputWrapper}>
              <FontAwesome5 name="lock" size={20} style={styles.IconInput} />
              <TextInput
                name="password"
                secureTextEntry={Unseenpass}
                placeholder="Password"
                style={styles.textInput}
                onChangeText={handleChange('password')}
                onBlur={handleBlur('password')}
                value={values.password}
              />
              <TouchableOpacity
                onPress={() => setUnseenpass(!Unseenpass)}
                style={styles.IconInput}>
                <FontAwesome5
                  name={Unseenpass ? 'eye-slash' : 'eye'}
                  size={20}
                />
              </TouchableOpacity>
            </View>
            <View style={styles.UnderInput}>
              <Switch
                style={styles.Switch}
                trackColor={{false: 'gray', true: '#5669FF'}}
                thumbColor={toggle ? '#FFFFFF' : '#FFFFFF'}
                onValueChange={Remember}
                value={toggle}></Switch>
              <CustomText
                content={'Remember me?'}
                styles={{
                  flex: 3,
                  alignSelf: 'center',
                  color: '#120D26',
                  fontSize: 14,
                }}
              />
              <TouchableOpacity
                onPress={handleForgotPassword}
                style={{alignSelf: 'center'}}>
                <CustomText
                  content={'Forgot Password?'}
                  styles={{color: '#120D26', fontSize: 14}}
                />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={styles.SignInButton}
              onPress={handleSubmit}>
              <CustomText
                content={'SIGN IN'}
                styles={styles.SignInButtonText}
              />
              {fetching === true ? (
                <Loading />
              ) : (
                <FontAwesome5
                  name="arrow-circle-right"
                  size={25}
                  color="#ccc"
                  style={styles.IconInput}
                />
              )}
            </TouchableOpacity>
          </View>
        )}
      </Formik>

      <View>
        <CustomText
          content={'OR'}
          styles={{
            textAlign: 'center',
            fontSize: 20,
            marginBottom: 17,
            marginTop: 20,
            color: '#9D9898',
          }}
        />
        <TouchableOpacity style={styles.ButtonOther} onPress={GoogleLog}>
          <CustomText
            content={'Login with Google'}
            styles={styles.ButtonOtherText}
          />
          {/* <FontAwesome5 name="google" size={27} style={styles.IconInput} /> */}
          <Image source={images.google} style={styles.LogoInput} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.ButtonOther} onPress={FacebookLog}>
          <CustomText
            content={'Login with Facebook'}
            styles={styles.ButtonOtherText}
          />
          <FontAwesome5
            name="facebook"
            size={28}
            style={styles.LogoInput}
            color="blue"
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{flexDirection: 'row', alignSelf: 'center', marginTop: 3}}
          onPress={() =>
            navigation.navigate('Register', {headerStyle: 'center'})
          }>
          <CustomText
            content={"Don't have an account?"}
            styles={{color: '#120D26', fontSize: 15}}
          />
          <CustomText
            content={' Sign up'}
            styles={{color: '#5669FF', fontSize: 15}}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 28,
    paddingRight: 28,
    backgroundColor: 'white',
    flex: 1,
  },
  Logo: {
    textAlign: 'center',
    marginTop: 20,
    marginBottom: 7,
  },
  AppName: {
    fontSize: 35,
    textAlign: 'center',
    marginBottom: 28,
    color: '#37364A',
  },
  SignIn: {
    fontSize: 30,
    color: '#120D26',
    paddingLeft: 8,
    fontWeight: '700',
  },
  InputWrapper: {
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 8,
    borderColor: '#E4DFDF',
  },
  InputInfor: {
    height: 15,
    fontSize: 14,
    paddingRight: 4,
    alignSelf: 'flex-end',
    color: 'red',
  },
  IconInput: {
    alignSelf: 'center',
    marginLeft: 15,
    marginRight: 15,
  },
  LogoInput: {
    height: 30,
    width: 30,
    alignSelf: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
  textInput: {
    flex: 3,
    fontSize: 18,
    height: 56,
    width: 317,
  },
  UnderInput: {
    paddingLeft: 16,
    flexDirection: 'row',
    marginBottom: 27,
    marginTop: 10,
  },
  Switch: {
    width: 30,
  },
  SignInButton: {
    flexDirection: 'row',
    height: 58,
    width: 271,
    backgroundColor: '#5669FF',
    alignSelf: 'center',
    borderRadius: 18,
  },
  SignInButtonText: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'white',
    flex: 1,
    paddingLeft: '35%',
  },
  ButtonOther: {
    height: 56,
    width: 273,
    flexDirection: 'row-reverse',
    backgroundColor: '#D3D3D3',
    alignSelf: 'center',
    borderRadius: 15,
    marginBottom: 17,
  },
  ButtonOtherText: {
    flex: 2,
    fontSize: 15,
    alignSelf: 'center',
    color: 'black',
    textAlign: 'center',
    paddingRight: 30,
  },
});
