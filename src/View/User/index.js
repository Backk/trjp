import React from 'react';
import {useContext} from 'react';
import {AuthContext} from '../../context/AuthContext';
import {Text, SafeAreaView, Button} from 'react-native';

function User({navigation}) {
  const {logout, authState} = useContext(AuthContext);

  const handleLogout = async () => {
    try {
      await logout();
      navigation.navigate('Home');
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <SafeAreaView>
      <Text>User view</Text>
      <Button
        title="Tạo sự kiện"
        onPress={() =>
          navigation.navigate('CreateEvent', {name: 'Tạo sự kiện'})
        }
      />
      <Button title="Logout" onPress={() => handleLogout()} />
    </SafeAreaView>
  );
}

export default User;
