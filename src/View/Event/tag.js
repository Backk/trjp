import {View, StyleSheet, Text} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import CustomText from '../../components/CustomText';

function Tag({content, color, logo}) {
  return (
    <View style={{...styles.container, backgroundColor: color}}>
      <View style={styles.wrapper}>
        <FontAwesome5 name={logo} size={20} color="white" style={styles.logo} />
        <CustomText content={content} styles={styles.content} />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    width: 97,
    height: 40,
    borderRadius: 20,
    flexDirection: 'row',
    margin: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  wrapper: {
    display: 'flex',
    width: '85%',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  content: {
    color: 'white',
    fontSize: 14,
    fontWeight: '400',
    marginLeft: 4,
  },
  logo: {
    marginRight: 4,
  },
});

export default Tag;
