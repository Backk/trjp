import {
  View,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Text,
  Modal,
  ScrollView,
  Image,
} from 'react-native';
import {useState, useEffect} from 'react';
import CustomText from '../../components/CustomText';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import dayjs from 'dayjs';
import Tag from '../../components/Tag/Tag';
import {Keyboard} from 'react-native';

const tags = [
  {tagName: 'Sport', backgroundColor: '#F0635A', icon: 'volleyball-ball'},
  {tagName: 'Music', backgroundColor: '#F59762', icon: 'music'},
  {tagName: 'Food', backgroundColor: '#29D697', icon: 'utensils'},
  {tagName: 'Culture', backgroundColor: '#46CDFB', icon: 'yin-yang'},
];

const img =
  'https://static2.yan.vn/YanNews/2167221/202003/wibu-la-gi-tai-sao-wibu-bi-ki-thi-4bfcdcd0.png';

function CreateEvent() {
  const [eventTagNames, setEventTagNames] = useState([]);
  const [name, setName] = useState('');
  const [location, setLocation] = useState('');
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isTimePickerVisible, setTimePickerVisibility] = useState(false);
  const [isDurationPickerVisible, setDurationPickerVisibility] =
    useState(false);
  const [isDateChosen, setDateChosen] = useState(false);
  const [isTimeChosen, setTimeChosen] = useState(false);
  const [isDurationChosen, setDurationChosen] = useState(false);
  const [locationBox, setLocationBox] = useState(true);
  const [tagModalVisible, setTagModalVisible] = useState(false);
  const [writeCnt, setWriteCnt] = useState('');
  const [enventName, setEventName] = useState('');
  const [eventDate, setEventDate] = useState('');
  const [startTime, setStartTime] = useState('');
  const [endTime, setEndTime] = useState('');

  const handleChangeTag = tag => {
    const isExist = eventTagNames.filter(e => e.tagName === tag.tagName);
    if (isExist.length === 0) {
      setEventTagNames(pre => [...pre, tag]);
    } else {
      const temp = eventTagNames.filter(e => e.tagName !== tag.tagName);
      setEventTagNames(temp);
    }
  };

  const handleDatePicker = date => {
    day = dayjs(date).format('DD/MM/YYYY');
    setEventDate(day);
    setDateChosen(true);
    setDatePickerVisibility(false);
  };

  const handleTimePicker = time => {
    timeStart = dayjs(time).format('HH:mm');
    setTimeChosen(true);
    setTimePickerVisibility(false);
  };

  const handleDurationPicker = time => {
    timeEnd = dayjs(time).format('HH:mm');
    setDurationChosen(true);
    setDurationPickerVisibility(false);
  };

  const handleLocationChecker = () => {
    setLocationBox(!locationBox);
  };
  const handleSetLocation = () => {
    console.log(location);
  };

  const handleChange = e => {
    console.log(e.target.value);
  };

  return (
    <ScrollView
      style={{
        backgroundColor: 'white',
        paddingHorizontal: 20,
      }}>
      <CustomText
        content="Event Name"
        styles={{...styles.title, marginTop: 10}}
      />
      <View style={styles.titleInputWrapper}>
        <TextInput
          style={styles.titleInput}
          onChangeText={text => setEventName(text)}
        />
      </View>
      <View style={styles.eventDate}>
        <View style={styles.date}>
          <CustomText content="Date" styles={styles.timeText} />
          <TouchableOpacity
            style={styles.timeHolder}
            onPress={() => setDatePickerVisibility(true)}>
            {isDateChosen ? <Text>{day}</Text> : <Text>DD/MM/YY</Text>}
            <FontAwesome5 name="calendar" size={20} />
            <DateTimePickerModal
              isVisible={isDatePickerVisible}
              mode="date"
              onConfirm={handleDatePicker}
              onCancel={() => setDatePickerVisibility(false)}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.eventStartTime}>
          <CustomText content="From" styles={styles.timeText} />
          <TouchableOpacity
            style={styles.timeHolder}
            onPress={() => setTimePickerVisibility(true)}>
            {isTimeChosen ? <Text>{timeStart}</Text> : <Text>HH/MM</Text>}
            <FontAwesome5 name="clock" size={20} />
            <DateTimePickerModal
              isVisible={isTimePickerVisible}
              mode="time"
              is24Hour
              onConfirm={handleTimePicker}
              onCancel={() => setTimePickerVisibility(false)}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.eventEndTime}>
          <CustomText content="To" styles={styles.timeText} />
          <TouchableOpacity
            style={styles.timeHolder}
            onPress={() => setDurationPickerVisibility(true)}>
            {isDurationChosen ? <Text>{timeEnd}</Text> : <Text>HH/MM</Text>}
            <FontAwesome5 name="clock" size={20} />
            <DateTimePickerModal
              isVisible={isDurationPickerVisible}
              mode="time"
              is24Hour
              onConfirm={handleDurationPicker}
              onCancel={() => setDurationPickerVisibility(false)}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.timeNote}>
        {isDateChosen && isTimeChosen && isDurationChosen && (
          <>
            <FontAwesome5 name="check-circle" color="green" />
            <Text style={{...styles.timeNoteText, color: 'green'}}>
              {' '}
              Event day: {day} from {timeStart} to {timeEnd}
            </Text>
          </>
        )}
      </View>

      <CustomText content="Location" styles={styles.title} />
      <View style={styles.titleInputWrapper}>
        <TextInput
          style={styles.titleInput}
          onChangeText={e => setLocation(e)}
          placeholder="Location"
        />
      </View>
      <View style={styles.locationCheckBoxWrapper}>
        <TouchableOpacity
          style={styles.locationCheckBox}
          onPress={handleLocationChecker}>
          {locationBox && <FontAwesome5 name="check" color={'#3F38DD'} />}
        </TouchableOpacity>
        <CustomText content=" Use recent GPS location" />
      </View>

      <CustomText content="Tag" styles={styles.title} />
      <View style={styles.tag}>
        {eventTagNames.map((eventTagName, index) => {
          return (
            <TouchableOpacity
              key={`${eventTagName.tagName}${Math.random() * 200 + 1}`}
              onPress={() => handleChangeTag(eventTagName)}>
              <Tag
                tagName={eventTagName.tagName}
                icon={eventTagName.icon}
                backgroundColor={eventTagName.backgroundColor}
                style={{
                  borderRadius: 10,
                  width: 100,
                  height: 30,
                  marginLeft: 0,
                  marginBottom: 5,
                }}
              />
            </TouchableOpacity>
          );
        })}
        <TouchableOpacity
          style={{
            backgroundColor: '#3F38DD',
            borderRadius: 100,
            width: 40,
            height: 30,
            marginLeft: 0,
            marginBottom: 5,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => setTagModalVisible(true)}>
          <FontAwesome5 name="plus" color="white" />
        </TouchableOpacity>
      </View>

      <View>
        <CustomText content="Decription" styles={styles.title} />
        <TextInput
          style={styles.description}
          keyboardType="default"
          returnKeyType="done"
          multiline={true}
          blurOnSubmit={true}
          onSubmitEditing={() => {
            Keyboard.dismiss();
          }}
          onChangeText={e => setWriteCnt(e)}
        />
      </View>
      <View style={{marginVertical: 10}}>
        {img ? (
          <View style={{position: 'relative'}}>
            <Image
              source={{uri: img}}
              style={{
                width: '100%',
                height: 200,
                borderRadius: 10,
              }}
            />
            <TouchableOpacity style={{position: 'absolute', right: 5, top: 10}}>
              <FontAwesome5 name="edit" color="#3F38DD" size={20} />
            </TouchableOpacity>
          </View>
        ) : (
          <TouchableOpacity
            style={{
              backgroundColor: 'lightgray',
              borderRadius: 10,
              height: 200,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <FontAwesome5 name="image" size={100} />
          </TouchableOpacity>
        )}
      </View>
      <Modal animationType="slide" transparent={true} visible={tagModalVisible}>
        <View style={styles.ModalView}>
          <View style={styles.ContentView}>
            {tags.map((tag, index) => (
              <TouchableOpacity
                style={{marginTop: 5}}
                key={tag.tagName}
                onPress={() => handleChangeTag(tag)}>
                <Tag
                  key={tag.tagName}
                  tagName={tag.tagName}
                  icon={tag.icon}
                  backgroundColor={
                    eventTagNames.includes(tag) ? tag.backgroundColor : 'gray'
                  }
                />
              </TouchableOpacity>
            ))}
          </View>
          <TouchableOpacity
            style={styles.ModalCloseButton}
            onPress={() => setTagModalVisible(false)}>
            <CustomText
              content="Close"
              styles={{...styles.addDesButton, color: 'white'}}
            />
          </TouchableOpacity>
        </View>
      </Modal>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    marginBottom: 5,
    color: '#3F38DD',
  },
  titleInputWrapper: {
    width: '100%',
    height: 40,
    alignSelf: 'center',
    borderRadius: 14,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: '#E4DFDF',
  },
  titleInput: {
    paddingLeft: 10,
    width: '60%',
  },
  titleAddButton: {
    width: '38%',
    height: 34,
    backgroundColor: '#D3D3D3',
    borderRadius: 8,
    marginRight: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  addDesButton: {},
  eventDate: {
    height: 70,
    width: '90%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 5,
  },
  date: {
    height: '100%',
    width: '38%',
  },
  eventStartTime: {
    height: '100%',
    width: '28%',
  },
  eventEndTime: {
    height: '100%',
    width: '28%',
  },
  timeText: {
    fontSize: 20,
    marginBottom: 5,
    color: '#3F38DD',
  },
  timeHolder: {
    height: '60%',
    width: '100%',
    backgroundColor: '#D3D3D3',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    borderRadius: 10,
    flexDirection: 'row',
  },
  timeNote: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5,
  },
  timeNoteText: {
    color: '#3F38DD',
  },
  locationCheckBoxWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
  },
  locationCheckBox: {
    height: 15,
    width: 15,
    borderWidth: 1,
    borderColor: '#E4DFDF',
    borderRadius: 5,
  },
  notificationWrapper: {
    height: '60%',
    width: '100%',
    backgroundColor: '#D3D3D3',
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 10,
  },
  notificationToggle: {
    height: '90%',
    width: '45%',
    borderRadius: 9,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgWrapper: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  eventDetail: {
    justifyContent: 'space-around',
    marginTop: 10,
  },
  tag: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  ModalView: {
    height: '100%',
    width: '100%',
    backgroundColor: 'gray',
    opacity: 0.95,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ContentView: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 100,
    flexWrap: 'wrap',
  },
  ModalCloseButton: {
    width: '38%',
    height: 34,
    borderRadius: 8,
    marginRight: 3,
    backgroundColor: '#4A43EC',
    justifyContent: 'center',
    alignItems: 'center',
  },
  description: {
    width: '100%',
    height: 300,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#E4DFDF',
    borderRadius: 10,
    textAlignVertical: 'top',
  },
  confirmButton: {
    width: '50%',
    height: 40,
    backgroundColor: '#3F38DD',
    borderRadius: 8,
    marginRight: 3,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 20,
  },
});

export default CreateEvent;
