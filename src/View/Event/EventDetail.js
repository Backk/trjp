import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
  Image,
  ScrollView,
  Linking,
  Platform,
  Alert,
} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import CustomText from '../../components/CustomText';
import icons from '../../assets/icons/icons';
import {useContext, useEffect, useState} from 'react';
import {AuthContext} from '../../context/AuthContext';
import images from '../../assets/images/images';
import dayjs from 'dayjs';
import weekday from 'dayjs/plugin/weekday';
import checkValidTime from '../../utils/checkValidTime';
import Loading from '../../components/Loading/Loading';
import placeApi from '../../api/placesApi';
dayjs.extend(weekday);

const eventTime = [
  {
    name: 'Monday',
    from: '08:00',
    to: '17:00',
  },
  {
    name: 'Tuesday',
    from: '08:00',
    to: '17:00',
  },
  {
    name: 'Wednesday',
    from: '08:00',
    to: '17:00',
  },
  {
    name: 'Thursday',
    from: '08:00',
    to: '17:00',
  },
  {
    name: 'Friday',
    from: '08:00',
    to: '17:00',
  },
  {
    name: 'Saturday',
    from: '08:00',
    to: '17:00',
  },
  {
    name: 'Sunday',
    from: '08:00',
    to: '17:00',
  },
];

function EventDetail({route, navigation}) {
  const [eventInfo, setEventInfo] = useState(route.params.eventInfo);
  const [reload, setReload] = useState(false);
  const [showEventTime, setShowEventTime] = useState(false);
  const isValidTime = checkValidTime();
  const [fetching, setFetching] = useState(false);
  const [today, setToday] = useState(() => {
    const _weekday = dayjs().weekday();
    if (_weekday - 1 < 0) return eventTime[6];
    return eventTime[_weekday - 1];
  });
  const {
    authState: {isAuthenticated, user},
  } = useContext(AuthContext);

  const handleOpenMap = () => {
    const scheme = Platform.select({ios: 'maps:0,0?q=', android: 'geo:0,0?q='});
    const latLng = `${eventInfo?.coordinates[1]}, ${eventInfo?.coordinates[0]}`;
    const label = `${eventInfo?.name}`;
    const url = Platform.select({
      ios: `${scheme}${label}@${latLng}`,
      android: `${scheme}${latLng}(${label})`,
    });

    Linking.openURL(url);
  };

  console.log(eventInfo?._id);

  const buyTicket = async () => {
    if (!eventInfo._id) {
      Alert.alert('Đây là data test');
      return;
    }
    if (isValidTime === false) {
      Alert.alert('Chức năng mua vé trước đang được phát triển');
      return;
    }

    if (isAuthenticated === false) {
      Alert.alert('chưa đăng nhập');
      navigation.navigate('Login');
      return;
    }

    setFetching(true);
    try {
      const res = await placeApi.buyTicket({
        placeId: eventInfo._id,
        userId: user._id,
      });
      if (res.data.success === true) {
        Alert.alert('Mua vé thành công!');
        setFetching(false);
      }
    } catch (er) {
      console.log(er);
      setFetching(false);
    } finally {
      setFetching(false);
      setReload(!reload);
    }
    // Alert.alert('Mua vé thành công');
  };

  const getPlaceById = async () => {
    try {
      const res = await placeApi.getPlaceById(eventInfo?._id);
      // console.log(res.data.data);
      setEventInfo(res.data.data);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    eventInfo._id && getPlaceById();
  }, [reload]);

  return (
    <View style={{backgroundColor: 'white', flex: 1, position: 'relative'}}>
      {/* header */}
      <View style={{position: 'relative', height: 180}}>
        <ImageBackground
          source={
            eventInfo?.thumbnail ? {uri: eventInfo?.thumbnail} : images.langBac
          }
          style={styles.headerImage}
        />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            padding: 10,
            justifyContent: 'space-between',
          }}>
          <TouchableOpacity
            style={styles.headerBtn}
            onPress={() => navigation.goBack()}>
            <FontAwesome5 color={'white'} name={'arrow-left'} size={23} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.headerBtn}
            onPress={() => console.log('gogo')}>
            <FontAwesome5 color={'white'} name={'heart'} size={23} />
          </TouchableOpacity>
        </View>
        <View style={styles.participants}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image
              style={styles.tinyImg1}
              source={{
                uri: 'https://i.scdn.co/image/ab6761610000e5eb4e2e2c78de847c4d9b12d32f',
              }}
            />
            <Image
              style={styles.tinyImg1}
              source={{
                uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeS6UkJc5-yibdsICv6l-O3-QpGiWuN3DOrA&usqp=CAU',
              }}
            />
            <Image
              style={styles.tinyImg1}
              source={{
                uri: 'https://avatar-ex-swe.nixcdn.com/singer/avatar/2018/07/19/8/1/6/a/1531967352055_600.jpg',
              }}
            />
            <CustomText content="+20 Going" styles={styles.ongoingText} />
          </View>
          <TouchableOpacity
            style={styles.inviteBtn}
            onPress={() => console.log('Mời')}>
            <Text style={{color: 'white'}}>Invite</Text>
          </TouchableOpacity>
        </View>
      </View>

      {/* body */}
      <ScrollView style={{position: 'relative'}}>
        <View style={{paddingHorizontal: 20, marginBottom: 10}}>
          <CustomText
            content={eventInfo?.name || 'test'}
            styles={{
              fontSize: 35,
              lineHeight: 46,
              marginBottom: 20,
              marginTop: 40,
            }}
          />

          <View style={styles.EventTime}>
            <View
              style={{
                backgroundColor: '#eae9fc',
                padding: 5,
                borderRadius: 10,
                marginRight: 10,
              }}>
              <Image
                source={icons.LocationIcon}
                style={{width: 42, height: 42}}
              />
            </View>
            <View>
              <View style={{flexDirection: 'row'}}>
                <CustomText
                  numberOfLines={1}
                  styles={{fontWeight: 'bold', width: 200, fontSize: 17}}
                  content={eventInfo?.addressLine1 || 'test'}
                />
                <TouchableOpacity
                  onPress={() => handleOpenMap()}
                  style={{
                    backgroundColor: '#eae9fc',
                    marginLeft: 20,
                    paddingHorizontal: 10,
                    paddingVertical: 4,
                    borderRadius: 10,
                  }}>
                  <CustomText
                    styles={{
                      color: '#3F38DD',
                      fontWeight: 'bold',
                      fontSize: 10,
                    }}
                    content={'Open Map'}
                  />
                </TouchableOpacity>
              </View>
              <CustomText
                styles={{color: 'gray', fontSize: 12, width: 300}}
                content={eventInfo?.addressLine2 || 'test'}
              />
            </View>
          </View>

          <View style={{...styles.EventTime}}>
            <View
              style={{
                backgroundColor: '#eae9fc',
                padding: 5,
                borderRadius: 10,
                marginRight: 10,
              }}>
              <Image
                source={icons.CalendarIcon}
                style={{width: 42, height: 42}}
              />
            </View>
            <View>
              <View style={{flexDirection: 'row'}}>
                <CustomText
                  styles={{fontWeight: 'bold', fontSize: 17}}
                  content={dayjs(Date.now()).format('DD MMMM, YYYY')}
                />
                <TouchableOpacity
                  onPress={() => setShowEventTime(!showEventTime)}
                  style={{
                    backgroundColor: '#eae9fc',
                    marginLeft: 20,
                    paddingHorizontal: 10,
                    paddingVertical: 4,
                    borderRadius: 10,
                  }}>
                  <CustomText
                    styles={{
                      color: '#3F38DD',
                      fontWeight: 'bold',
                      fontSize: 10,
                    }}
                    content={showEventTime ? 'Collapse' : 'See more'}
                  />
                </TouchableOpacity>
              </View>

              <CustomText
                styles={{color: 'gray', fontSize: 15}}
                content={`${today.name}, ${today.from} - ${today.to}`}
              />
            </View>
          </View>
          <View style={{marginLeft: 60}}>
            {showEventTime &&
              eventTime.map((item, index) => {
                if (item.name === today.name) {
                  return (
                    <CustomText
                      key={item.name}
                      styles={{
                        color: '#3F38DD',
                        fontSize: 17,
                        marginBottom: 5,
                        fontWeight: 'bold',
                      }}
                      content={`${item.name}: ${item.from} - ${item.to}`}
                    />
                  );
                }
                return (
                  <CustomText
                    key={item.name}
                    styles={{color: 'black', fontSize: 17, marginBottom: 5}}
                    content={`${item.name}: ${item.from} - ${item.to}`}
                  />
                );
              })}
          </View>

          <View style={styles.EventTime}>
            {/* <View
              style={{
                backgroundColor: '#eae9fc',
                padding: 5,
                borderRadius: 10,
                marginRight: 10,
              }}>
              <Image source={icons.LocationIcon} />
            </View> */}
            {/* <View>
              <View style={{flexDirection: 'row'}}>
                <CustomText
                  styles={{fontWeight: 'bold'}}
                  content={'FullStuck'}
                />
                <TouchableOpacity
                  onPress={() => console.log('follow')}
                  style={{
                    backgroundColor: '#eae9fc',
                    marginLeft: 20,
                    paddingHorizontal: 10,
                    paddingVertical: 4,
                    borderRadius: 10,
                  }}>
                  <CustomText
                    styles={{
                      color: '#3F38DD',
                      fontWeight: 'bold',
                      fontSize: 10,
                    }}
                    content={'Follow'}
                  />
                </TouchableOpacity>
              </View>
              <CustomText
                styles={{color: 'gray', fontSize: 10}}
                content={'Organizer'}
              />
            </View> */}
          </View>

          <View>
            <CustomText
              styles={{fontWeight: 'bold', fontSize: 17}}
              content={'Mô tả'}
            />
            <CustomText
              content={
                "Lorem Ipsum is  Ipsum is simply dummy te Ipsum is simply dummy te Ipsum is simply dummy te Ipsum is simply dummy te Ipsum is simply dummy tesimply dummy te Ipsum is simply dummy te Ipsum is simply dummy te Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
              }
              styles={{lineHeight: 20}}
            />
          </View>
        </View>
      </ScrollView>
      <TouchableOpacity
        style={{
          ...styles.buyTicketBtn,
          backgroundColor: '#3F38DD',
        }}
        onPress={() => buyTicket()}>
        {isValidTime === false ? (
          <Text
            style={{textAlign: 'center', color: 'white', fontWeight: 'bold'}}>
            {'Đặt vé trước'}
          </Text>
        ) : fetching === true ? (
          <Loading />
        ) : (
          <Text style={{textAlign: 'center', color: 'white'}}>
            {eventInfo?.tickets?.includes(user?._id)
              ? 'Đã mua vé'
              : 'Mua vé $120'}
          </Text>
        )}
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  headerImage: {
    width: '100%',
    height: 180,
    position: 'absolute',
  },
  headerBtn: {
    marginRight: 10,
    backgroundColor: '#3F38DD',
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  },
  participants: {
    width: '80%',
    alignSelf: 'center',
    marginTop: 5,
    height: 60,
    paddingLeft: 18,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around',
    position: 'absolute',
    bottom: -30,
    backgroundColor: 'white',
    borderRadius: 30,
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    elevation: 3,
    zIndex: 1000,
  },
  tinyImg1: {
    marginLeft: -10,
    height: 30,
    width: 30,
    borderRadius: 50,
  },
  ongoingText: {
    marginLeft: 10,
    color: '#3F38DD',
    fontWeight: '500',
    fontSize: 12,
    fontWeight: 'bold',
  },
  inviteBtn: {
    backgroundColor: '#3F38DD',
    paddingHorizontal: 20,
    paddingVertical: 5,
    borderRadius: 10,
  },
  EventTime: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  buyTicketBtn: {
    width: '80%',
    alignSelf: 'center',

    borderRadius: 10,
    height: 50,
    justifyContent: 'center',
    marginVertical: 5,
    // position: 'absolute',
    // bottom: 0,
  },
});

export default EventDetail;
