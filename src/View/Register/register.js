import React, {useState, useContext} from 'react';
import {Alert, View} from 'react-native';
import {
  SafeAreaView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import CustomText from '../../components/CustomText';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import images from '../../assets/images/images';
import Loading from '../../components/Loading/Loading';
import {AuthContext} from '../../context/AuthContext';

function Register({navigation: {navigate}}) {
  const [hidePass, setHidePass] = useState(true);
  const [hidePass2, setHidePass2] = useState(true);
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [phone, setPhone] = useState(null);
  const [confirmPassword, setConfirmPassword] = useState(null);
  const {register} = useContext(AuthContext);
  const [fetching, setFetching] = useState(false);

  const handleRegister = async () => {
    const data = {
      email,
      phone,
      password,
      confirmPassword,
    };
    if (Object.values(data).some(v => v === null || v === '')) {
      Alert.alert('Chưa nhập đầy đủ thông tin!!');
      return;
    }

    if (data.confirmPassword !== data.password) {
      Alert.alert('Mật khẩu chưa khớp!');
      return;
    }

    try {
      setFetching(true);
      const res = await register(data);
      if (res.success === true) {
        navigate('Homes');
      } else {
        Alert.alert(res.message);
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Đăng kí không thành công');
    } finally {
      setFetching(false);
    }
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.form}>
        <FontAwesome5 name="envelope" size={20} style={styles.iconInput} />
        <TextInput
          style={styles.input1}
          onChangeText={text => setEmail(text)}
          placeholder="Email"
          placeholderTextColor={'#74768'}></TextInput>
      </View>
      <View style={styles.form}>
        <FontAwesome5 name="phone" size={20} style={styles.iconInput} />
        <TextInput
          style={styles.input1}
          onChangeText={text => setPhone(text)}
          keyboardType="numeric"
          placeholder="Số điện thoại"></TextInput>
      </View>
      <View style={styles.form}>
        <View style={{display: 'flex', flexDirection: 'row'}}>
          <FontAwesome5 name="user-lock" size={20} style={styles.iconInput} />
          <TextInput
            style={styles.input2}
            onChangeText={text => setPassword(text)}
            placeholder="Your password"
            secureTextEntry={hidePass ? true : false}></TextInput>
        </View>
        <TouchableOpacity
          onPress={() => setHidePass(!hidePass)}
          style={styles.iconInput}>
          <FontAwesome5 name={hidePass ? 'eye-slash' : 'eye'} size={20} />
        </TouchableOpacity>
      </View>
      <View style={styles.form}>
        <View style={{display: 'flex', flexDirection: 'row'}}>
          <FontAwesome5 name="user-lock" size={20} style={styles.iconInput} />
          <TextInput
            onChangeText={text => setConfirmPassword(text)}
            style={styles.input2}
            placeholder="Confirm password"
            secureTextEntry={hidePass2 ? true : false}></TextInput>
        </View>
        <TouchableOpacity
          onPress={() => setHidePass2(!hidePass2)}
          style={styles.iconInput}>
          <FontAwesome5 name={hidePass2 ? 'eye-slash' : 'eye'} size={20} />
        </TouchableOpacity>
      </View>
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-around',
          backgroundColor: '#5669FF',
          borderRadius: 18,
          width: 271,
          height: 58,
          marginLeft: '15%',
          marginTop: 23,
        }}>
        <TouchableOpacity onPress={handleRegister}>
          <CustomText
            content={'SIGN UP'}
            styles={{
              textAlign: 'center',
              paddingLeft: 80,
              color: '#fff',
              fontSize: 20,
              marginRight: '15%',
            }}
          />
        </TouchableOpacity>
        {fetching === false ? (
          <FontAwesome5
            name="arrow-circle-right"
            size={25}
            style={{color: '#ccc'}}
          />
        ) : (
          <Loading />
        )}
      </View>
      <CustomText
        content={'OR'}
        styles={{
          textAlign: 'center',
          fontSize: 20,
          marginTop: 20,
          marginBottom: 17,
          color: '#9D9898',
        }}
      />
      <TouchableOpacity style={styles.ButtonOther}>
        <CustomText
          content={'Login with Google'}
          styles={styles.ButtonOtherText}
        />
        {/* <FontAwesome5 name="google" size={27} style={styles.IconInput} /> */}
        <Image source={images.google} style={styles.LogoInput} />
      </TouchableOpacity>
      <TouchableOpacity style={styles.ButtonOther}>
        <CustomText
          content={'Login with Facebook'}
          styles={styles.ButtonOtherText}
        />
        <FontAwesome5
          name="facebook"
          size={28}
          style={styles.LogoInput}
          color="blue"
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={{flexDirection: 'row', alignSelf: 'center', marginTop: 3}}>
        <CustomText
          content={'Already have an account?'}
          styles={{color: '#120D26', fontSize: 15}}
        />
        <CustomText
          content={' Sign in'}
          styles={{color: '#5669FF', fontSize: 15}}
        />
      </TouchableOpacity>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 40,
  },
  form: {
    color: '#fff',
    height: 56,
    width: '80%',
    flexDirection: 'row',
    borderColor: '#E4DFDF',
    paddingLeft: 10,
    borderWidth: 1,
    borderRadius: 8,
    marginLeft: '10%',
    marginBottom: 20,
  },
  input1: {
    width: 255,
  },
  input2: {
    width: 210,
  },
  iconInput: {
    color: '#807A7A',
    marginLeft: 5,
    marginRight: 15,
    // backgroundColor:"green",
    alignSelf: 'center',
  },
  ButtonOther: {
    height: 56,
    width: 273,
    flexDirection: 'row-reverse',
    backgroundColor: '#D3D3D3',
    alignSelf: 'center',
    borderRadius: 15,
    marginBottom: 17,
  },
  ButtonOtherText: {
    flex: 2,
    fontSize: 15,
    alignSelf: 'center',
    color: 'black',
    textAlign: 'center',
    paddingRight: 30,
  },
  LogoInput: {
    height: 30,
    width: 30,
    alignSelf: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
});

export default Register;
