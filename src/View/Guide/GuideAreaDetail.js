import {useEffect} from 'react';
import {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ScrollView,
  ImageBackground,
} from 'react-native';
import placesApi from '../../api/placesApi';
import SinglePlace from './component/SinglePlace';
import SkeletonSinglePlace from './component/SkeletonSinglePlace';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const GuideAreaDetail = ({navigation, route}) => {
  const {city} = route.params;
  const [foods, setFood] = useState([]);
  const [entertainments, setEntertainments] = useState([]);
  const [hotels, setHotels] = useState([]);
  const [fetching, setFetching] = useState(false);

  const getData = async () => {
    setFetching(true);
    try {
      const foodsRes = await placesApi.getPlaces({
        cityId: city._id,
        type: 'food',
      });
      const EnterRes = await placesApi.getPlaces({
        cityId: city._id,
        type: 'entertainment',
      });
      const hotelsRes = await placesApi.getPlaces({
        cityId: city._id,
        type: 'hotel',
      });

      setFood(foodsRes.data.data);
      setEntertainments(EnterRes.data.data);
      setHotels(hotelsRes.data.data);
      setFetching(false);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    getData();
  }, []);

  return (
    <ScrollView style={{backgroundColor: '#fff'}}>
      <View style={{position: 'relative'}}>
        <TouchableOpacity
          style={styles.headerBtn}
          onPress={() => navigation.goBack()}>
          <FontAwesome5 color={'white'} name={'arrow-left'} size={23} />
        </TouchableOpacity>
        <ImageBackground
          source={{uri: city.thumbnail}}
          style={{width: '100%', height: 190}}
        />
        <Text
          style={{
            fontSize: 25,
            color: 'black',
            fontWeight: 'bold',
            margin: 10,
          }}>
          {city.title}
        </Text>
      </View>

      <View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text style={styles.textHeaderLeft}>Vui chơi</Text>
          <TouchableOpacity>
            <Text style={styles.textHeaderRight}>Xem tất cả</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.textDescribe}>
          Địa điểm tham quan, hoạt động khám phá và những trải nghiệm tiêu biểu.
        </Text>
        {fetching === true ? (
          <SkeletonSinglePlace />
        ) : (
          <SinglePlace items={entertainments} navigation={navigation} />
        )}
      </View>

      <View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text style={styles.textHeaderLeft}>Lưu trú</Text>
          <TouchableOpacity>
            <Text style={styles.textHeaderRight}>Xem tất cả</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.textDescribe}>
          Nơi nghỉ dưỡng với sự kết hợp của nét quyến rũ, vẻ hiện đại và tính
          chân thực.
        </Text>
        {fetching === true ? (
          <SkeletonSinglePlace />
        ) : (
          <SinglePlace items={hotels} navigation={navigation} />
        )}
      </View>

      <View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text style={styles.textHeaderLeft}>Dùng bữa</Text>
          <TouchableOpacity>
            <Text style={styles.textHeaderRight}>Xem tất cả</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.textDescribe}>
          Những địa điểm ăn uống và thưởng thức tiệc không thể bỏ lỡ.
        </Text>
        {fetching === true ? (
          <SkeletonSinglePlace />
        ) : (
          <SinglePlace items={foods} navigation={navigation} />
        )}
      </View>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  headerBtn: {
    backgroundColor: '#3F38DD',
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    position: 'absolute',
    top: 10,
    left: 10,
    zIndex: 100,
  },
  textHeaderLeft: {
    fontSize: 20,
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'black',
  },
  textHeaderRight: {
    color: '#555',
    marginRight: 20,
    marginTop: 5,
    textDecorationLine: 'underline',
  },
  textDescribe: {
    marginTop: 5,
    color: 'black',
    fontSize: 14,
    marginLeft: 10,
    fontWeight: '500',
    marginRight: 20,
    letterSpacing: 0.7,
  },
  vote: {
    backgroundColor: 'green',
    width: 11,
    height: 11,
    borderRadius: 20,
    marginRight: 2,
  },
});
export default GuideAreaDetail;
