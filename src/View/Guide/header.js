import React from 'react';
import {View} from 'react-native';
import {StyleSheet, TextInput, TouchableOpacity, Image} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const Header = ({draw}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity>
        <FontAwesome5 name="arrow-left" size={25} style={{color: '#fff'}} />
      </TouchableOpacity>
      {/* <TouchableOpacity
          style={{borderRightWidth: 1, height: 25, marginRight: 20, paddingRight: 10, borderColor: '#fff'}}>
          <FontAwesome5
            name="bars"
            size={20}
            style={{ color: '#fff'}}></FontAwesome5>
        </TouchableOpacity> */}
      <View
        style={{
          flexDirection: 'row',
          borderColor: '#fff',
          borderWidth: 1,
          height: 30,
          width: 300,
          alignItems: 'center',
          borderRadius: 5,
        }}>
        <TouchableOpacity>
          <FontAwesome5
            name="search"
            size={20}
            style={{color: '#fff', marginLeft: 5}}
          />
        </TouchableOpacity>
        <TextInput
          placeholder="search..."
          style={{color: '#fff', height: 40, width: 270}}
          placeholderTextColor="#fff"></TextInput>
      </View>
      <View>
        <TouchableOpacity onPress={() => draw.current.openDrawer()}>
          <Image
            style={{height: 25, width: 25}}
            source={require('../../assets/images/filter.png')}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#4A43EC',
  },
  ContentBar: {
    top: 70,
    width: '100%',
    position: 'absolute',
    flexDirection: 'row',
  },
});
export default Header;
