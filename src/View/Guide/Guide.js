import React from 'react';
import {TextInput, TouchableOpacity, View} from 'react-native';
import {Text, StyleSheet, Image} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const listTops = [
  {
    id: 1,
    name: 'Tour Bái Đính - Tràng An - Hang múa',
    image: 'https://tourre.vn/wp-content/uploads/2020/11/chua-bai-dinh-2.jpg',
    vote: '10.762',
    tag: 'Chuyến tham quan ngắm cảnh',
    price: '929.412 đ/người',
  },
  {
    id: 2,
    name: 'Khám phá Vịnh Hạ Long bằng Du Thuyền',
    image:
      'https://image.thanhnien.vn/1200x630/Uploaded/2022/wsxrxqeiod/2019_08_23/halong-3_pids.jpg',
    vote: '12.412',
    tag: 'Chuyến tham quan phiêu lưu',
    price: '2.988.900 đ/người',
  },
  {
    id: 3,
    name: 'Tour Khu Di Tích Địa Đạo Củ Chi & Đồng Bằng Sông Cửu Long',
    image:
      'https://cafefcdn.com/203337114487263232/2021/4/21/photo-1-16189880756381588169593.jpg',
    vote: '7.264',
    tag: 'Chuyến tham quan ngắm cảnh',
    price: '1.429.305 đ/người',
  },
  {
    id: 4,
    name: 'Tour ẩm thực phố cổ',
    image:
      'https://spk.vn/wp-content/uploads/2021/04/kham-pha-ngay-nhung-net-dac-trung-cua-van-hoa-am-thuc-pho-co-hoi-an-am-thuc-hoi-an-wecheckin.jpg',
    vote: '2.232',
    tag: 'Ẩm thực đường phố',
    price: '404.297 đ/người',
  },
];
const listNearBy = [
  {
    id: 5,
    name: 'Thiên đường Bảo Sơn',
    image:
      'https://bizweb.dktcdn.net/100/366/377/articles/thien-duong-bao-son.jpg?v=1645072641677',
    tag: 'Công Viên',
    location: '2.4km',
    status: '8h-19h',
  },
  {
    id: 6,
    name: 'Vụn Art',
    image:
      'https://tranhghepvai.vn/wp-content/uploads/2019/07/gom-vun-ve-giac-mo-9.jpg',
    tag: 'Phòng trưng bày nghệ thuật',
    location: '2.9km',
    status: '7h-20h',
  },
  {
    id: 7,
    name: 'Đình Bia Bà',
    image:
      'https://bietthungoctrai.vn/wp-content/uploads/Dinh-Chua-Bia-Ba-La-Khe3.jpg',
    tag: 'Địa điểm tôn giáo',
    location: '3.9km',
    status: '7h-20h',
  },
];

const cities = [
  {
    _id: '639880332508ff098004c87a',
    title: 'Đà Nẵng',
    cors: [108.20224231542497, 16.054094618546678],
    thumbnail:
      'https://vcdn1-dulich.vnecdn.net/2022/06/01/CauVangDaNang-1654082224-7229-1654082320.jpg?w=0&h=0&q=100&dpr=2&fit=crop&s=MeVMb72UZA27ivcyB3s7Kg',
  },
  {
    _id: '6398801b2508ff098004c878',
    title: 'Hạ Long',
    cors: [107.0476722073505, 20.9705267277375],
    thumbnail:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeTC8xwKPV16Yyw9PyJGvwfvwN4IjJhIkGBQ&usqp=CAU',
  },
  {
    _id: '639880332508ff098004c87a',
    title: 'Đà Nẵng',
    cors: [108.20224231542497, 16.054094618546678],
    thumbnail:
      'https://vcdn1-dulich.vnecdn.net/2022/06/01/CauVangDaNang-1654082224-7229-1654082320.jpg?w=0&h=0&q=100&dpr=2&fit=crop&s=MeVMb72UZA27ivcyB3s7Kg',
  },
  {
    _id: '6398801b2508ff098004c878',
    title: 'Hạ Long',
    cors: [107.0476722073505, 20.9705267277375],
    thumbnail:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeTC8xwKPV16Yyw9PyJGvwfvwN4IjJhIkGBQ&usqp=CAU',
  },
];
const Guide = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#fff'}}>
      <ScrollView>
        <View>
          <Text
            style={{
              color: 'black',
              fontSize: 25,
              fontWeight: 'bold',
              marginLeft: 10,
              marginTop: 10,
            }}>
            Tìm kiếm
          </Text>
          <TouchableOpacity
            onPress={() => navigation.navigate('Search', {name: 'Tìm kiếm'})}
            style={{justifyContent: 'center', flexDirection: 'row'}}>
            <View
              style={{
                width: '90%',
                height: 45,
                borderColor: '#6f707d',
                borderWidth: 2,
                borderRadius: 20,
                alignItems: 'center',
                flexDirection: 'row',
                marginBottom: 20,
                marginTop: 20,
              }}>
              <FontAwesome5
                name="search"
                size={15}
                style={{marginLeft: 10}}></FontAwesome5>
              <TextInput
                editable={false}
                style={{height: 40}}
                placeholder="Bạn sắp đến đâu?"></TextInput>
            </View>
          </TouchableOpacity>
        </View>
        <View>
          <Text style={styles.textHeaderLeft}>Trải nghiệm hàng đầu</Text>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row'}}>
              {listTops.map((listTop, index) => {
                return (
                  <TouchableOpacity key={listTop.name}>
                    <View style={{margin: 10, marginRight: 5, marginLeft: 10}}>
                      <Image
                        source={{uri: listTop.image}}
                        style={{width: 220, height: 200}}
                      />
                      <Text
                        style={{
                          width: 220,
                          color: 'black',
                          fontWeight: 'bold',
                        }}>
                        {listTop.name}
                      </Text>
                      <View
                        style={{flexDirection: 'row', alignItems: 'center'}}>
                        <View style={styles.vote}></View>
                        <View style={styles.vote}></View>
                        <View style={styles.vote}></View>
                        <View style={styles.vote}></View>
                        <View style={styles.vote}></View>
                        <Text style={{fontSize: 11}}>{listTop.vote}</Text>
                      </View>
                      <Text style={{color: 'black', fontSize: 14}}>
                        {listTop.tag}
                      </Text>
                      <Text style={{color: 'black', fontSize: 14}}>
                        {listTop.price}
                      </Text>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </View>
          </ScrollView>
        </View>
        <View>
          <View style={styles.header}>
            <Text style={styles.textHeaderLeft}>Điểm du lịch gần bạn</Text>
            <Text style={styles.textHeaderRight}>Xem tất cả</Text>
          </View>
          {listNearBy.map((list, index) => {
            return (
              <TouchableOpacity key={list.name}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    source={{uri: list.image}}
                    style={{width: '30%', height: 80, margin: 10}}></Image>
                  <View>
                    <Text style={{color: 'black', marginTop: 10, fontSize: 16}}>
                      {list.name}
                    </Text>
                    <Text style={{color: 'black'}}>{list.tag}</Text>
                    <View
                      style={{
                        width: '60%',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{color: 'black', fontSize: 13, marginTop: 10}}>
                        {list.status}
                      </Text>
                      <Text
                        style={{
                          color: 'black',
                          fontSize: 12,
                          marginTop: 10,
                        }}>
                        {list.location}
                      </Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
        <View>
          <Text style={styles.textHeaderLeft}>
            Điểm đến được khách du lịch yêu thích
          </Text>
          <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
            {cities.map((city, index) => {
              return (
                <View
                  style={{flexDirection: 'row', width: '50%'}}
                  key={city.cityId + index.toString()}>
                  <TouchableOpacity
                    style={{width: '80%'}}
                    onPress={() => {
                      navigation.navigate('GuideAreaDetail', {city});
                    }}>
                    <Image
                      source={{uri: city.thumbnail}}
                      style={{
                        width: '100%',
                        height: 120,
                        flexDirection: 'row',
                        margin: 10,
                        position: 'relative',
                      }}
                    />
                  </TouchableOpacity>
                  <Text style={styles.textFavourite}>{city.title}</Text>
                </View>
              );
            })}
          </View>
        </View>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  textFavourite: {
    position: 'absolute',
    top: 90,
    left: 20,
    fontSize: 25,
    color: '#fff',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItem: 'center',
  },
  textHeaderLeft: {
    fontSize: 22,
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'black',
  },
  textHeaderRight: {
    color: '#888',
    marginRight: 10,
    marginTop: 5,
    textDecorationLine: 'underline',
  },
  vote: {
    backgroundColor: 'green',
    width: 11,
    height: 11,
    borderRadius: 20,
    marginRight: 2,
  },
});
export default Guide;
