import React from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';

const items = [1, 2, 3, 4, 5];

const SkeletonSinglePlace = () => {
  return (
    <ScrollView
      horizontal
      showsHorizontalScrollIndicator={false}
      style={{marginBottom: 20}}>
      {items.map((item, index) => {
        return (
          <View key={item}>
            <View style={{margin: 10, marginRight: 5, marginLeft: 10}}>
              <View
                style={{
                  width: 200,
                  height: 200,
                  backgroundColor: '#F1F2F3',
                }}></View>
              <View
                style={{
                  width: 200,
                  height: 10,
                  backgroundColor: '#F1F2F3',
                  marginVertical: 5,
                }}></View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={styles.vote}></View>
                <View style={styles.vote}></View>
                <View style={styles.vote}></View>
                <View style={styles.vote}></View>
                <View style={styles.vote}></View>
              </View>
              {/* <Text style={{color: 'black', fontSize: 14}}>
                      {play.tag}
                    </Text> */}
            </View>
          </View>
        );
      })}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  textHeaderLeft: {
    fontSize: 20,
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'black',
  },
  textHeaderRight: {
    color: '#555',
    marginRight: 20,
    marginTop: 5,
    textDecorationLine: 'underline',
  },
  textDescribe: {
    marginTop: 5,
    color: 'black',
    fontSize: 14,
    marginLeft: 10,
    fontWeight: '500',
    marginRight: 20,
    letterSpacing: 0.7,
  },
  vote: {
    backgroundColor: '#F1F2F3',
    width: 11,
    height: 11,
    borderRadius: 20,
    marginRight: 2,
  },
});

export default SkeletonSinglePlace;
