import React from 'react';
import images from '../../../assets/images/images';
import {
  TouchableOpacity,
  Text,
  Image,
  View,
  StyleSheet,
  ScrollView,
} from 'react-native';

const SinglePlace = ({items, navigation}) => {
  const goToDetail = eventInfo => {
    navigation.navigate('EventDetail', {eventInfo});
  };

  return (
    <ScrollView
      horizontal
      showsHorizontalScrollIndicator={false}
      style={{marginBottom: 20}}>
      {items.map((item, index) => {
        return (
          <TouchableOpacity key={item._id} onPress={() => goToDetail(item)}>
            <View style={{margin: 10, marginRight: 5, marginLeft: 10}}>
              <Image
                source={item.thumbnail ? {uri: item.thumbnail} : images.langBac}
                style={{width: 200, height: 200}}
              />
              <Text
                style={{
                  width: 200,
                  color: 'black',
                  fontWeight: 'bold',
                }}>
                {item.name}
              </Text>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={styles.vote}></View>
                <View style={styles.vote}></View>
                <View style={styles.vote}></View>
                <View style={styles.vote}></View>
                <View style={styles.vote}></View>
              </View>
              {/* <Text style={{color: 'black', fontSize: 14}}>
                      {play.tag}
                    </Text> */}
            </View>
          </TouchableOpacity>
        );
      })}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  textHeaderLeft: {
    fontSize: 20,
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'black',
  },
  textHeaderRight: {
    color: '#555',
    marginRight: 20,
    marginTop: 5,
    textDecorationLine: 'underline',
  },
  textDescribe: {
    marginTop: 5,
    color: 'black',
    fontSize: 14,
    marginLeft: 10,
    fontWeight: '500',
    marginRight: 20,
    letterSpacing: 0.7,
  },
  vote: {
    backgroundColor: 'green',
    width: 11,
    height: 11,
    borderRadius: 20,
    marginRight: 2,
  },
});

export default SinglePlace;
