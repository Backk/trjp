import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  ImageBackground,
  Image,
} from 'react-native';
import CustomText from '../../components/CustomText';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

function SingleEvent({
  day,
  month,
  imgsrc,
  name,
  ongoing,
  location,
  note,
  navigation,
}) {
  return (
    <TouchableWithoutFeedback
      onPress={() =>
        navigation.navigate('EventDetail', {
          eventInfo: {
            thumbnail: imgsrc,
            name,
            addressLine1: location,
            addressLine2: location,
            coordinates: [105.8228876, 20.963557600403664],
            tickets: [],
          },
        })
      }>
      <View style={styles.container}>
        <ImageBackground
          style={styles.image}
          imageStyle={{borderRadius: 10}}
          resizeMode="cover"
          source={{uri: imgsrc}}>
          <View style={styles.date}>
            <CustomText content={day} styles={styles.day} />
            <CustomText content={month} styles={styles.month} />
          </View>
          {note ? (
            <View style={styles.checkBox}>
              <FontAwesome5 name="bookmark" size={20} color="#F0635A" />
            </View>
          ) : null}
        </ImageBackground>
        <View style={styles.name}>
          <Text numberOfLines={1} ellipsizeMode="tail" style={styles.nameText}>
            {name}
          </Text>
        </View>
        <View style={styles.ongoing}>
          <CustomText content="+20" styles={styles.ongoingText} />
          <Image
            style={styles.tinyImg1}
            source={{
              uri: 'https://i.scdn.co/image/ab6761610000e5eb4e2e2c78de847c4d9b12d32f',
            }}
          />
          <Image
            style={styles.tinyImg2}
            source={{
              uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeS6UkJc5-yibdsICv6l-O3-QpGiWuN3DOrA&usqp=CAU',
            }}
          />
          <Image
            style={styles.tinyImg3}
            source={{
              uri: 'https://avatar-ex-swe.nixcdn.com/singer/avatar/2018/07/19/8/1/6/a/1531967352055_600.jpg',
            }}
          />
        </View>
        <View style={styles.loc}>
          <FontAwesome5 name="map-marker-alt" size={14} color="#716E90" />
          <Text
            numberOfLines={1}
            ellipsizeMode="tail"
            style={{
              color: '#2B2849',
              marginLeft: 7,
              fontSize: 13,
              fontWeight: '400',
            }}>
            {location}
          </Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}
const styles = StyleSheet.create({
  container: {
    width: 237,
    height: 255,
    marginLeft: 8,
    marginRight: 8,
    marginTop: 2,
    marginBottom: 2,
    borderRadius: 18,
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    elevation: 3,
    backgroundColor: 'white',
  },
  image: {
    marginLeft: 8,
    marginRight: 8,
    marginTop: 9,
    height: 131,
    flexDirection: 'row',
  },
  date: {
    left: 8,
    top: 8,
    width: 45,
    height: 45,
    backgroundColor: 'white',
    opacity: 0.85,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  day: {
    fontWeight: '800',
    fontSize: 17,
    color: '#F0635A',
    lineHeight: 17,
  },
  month: {
    fontWeight: '700',
    fontSize: 13,
    color: '#F0635A',
  },
  checkBox: {
    marginLeft: 138,
    top: 8,
    width: 30,
    height: 30,
    backgroundColor: 'white',
    opacity: 0.85,
    borderRadius: 7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  name: {
    alignSelf: 'center',
    marginTop: 14,
    width: '90%',
    height: 23,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  nameText: {
    color: 'black',
    fontSize: 18,
    fontWeight: '700',
  },
  ongoing: {
    width: '90%',
    alignSelf: 'center',
    marginTop: 5,
    height: 40,
    paddingLeft: 18,
    justifyContent: 'flex-end',
    alignItems: 'center',
    flexDirection: 'row-reverse',
  },
  ongoingText: {
    marginLeft: 10,
    color: '#3F38DD',
    fontWeight: '500',
    fontSize: 12,
  },
  tinyImg1: {
    marginLeft: -10,
    height: 24,
    width: 24,
    borderRadius: 50,
  },
  tinyImg2: {
    marginLeft: -10,
    height: 24,
    width: 24,
    borderRadius: 50,
  },
  tinyImg3: {
    height: 24,
    width: 24,
    borderRadius: 50,
  },
  loc: {
    marginTop: 3,
    height: 17,
    paddingLeft: 18,
    flexDirection: 'row',
    width: '90%',
  },
});
export default SingleEvent;
