import React, {useState} from 'react';
import {View, StyleSheet, ScrollView, TouchableOpacity} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import CustomText from '../../components/CustomText';
import Tag from './tag';
import SingleEvent from './event';
import {evenInfor, Highlights} from './fakeData';
import Geolocation from '@react-native-community/geolocation';
import {useEffect} from 'react';
import axios from 'axios';

const ContentArr = [
  {content: 'Sport', color: '#F0635A', logo: 'volleyball-ball'},
  {content: 'Music', color: '#F59762', logo: 'music'},
  {content: 'Food', color: '#29D697', logo: 'utensils'},
  {content: 'Culture', color: '#46CDFB', logo: 'yin-yang'},
];

function Home({navigation}) {
  const [currentPosition, setCurrentPosition] = useState([]);
  const [currentAddress, setCurrentAddress] = useState(null);

  const getCurrentAddress = async () => {
    if (currentPosition === []) return;

    try {
      const res = await axios.get(
        `https://api.geoapify.com/v1/geocode/reverse?lat=${currentPosition[0]}&lon=${currentPosition[1]}&apiKey=becfd172565140f3b633ab183a6f24c7`,
      );
      const temp = res.data?.features[0]?.properties;
      // console.log(temp);
      setCurrentAddress(
        [temp?.road, temp?.address_line1, temp?.country].join(', '),
      );
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    Geolocation.getCurrentPosition(info =>
      setCurrentPosition([info.coords.latitude, info.coords.longitude]),
    );
  }, []);

  useEffect(() => {
    getCurrentAddress();
  }, [currentPosition]);

  // console.log(currentAddress);
  return (
    <ScrollView>
      <View style={styles.Headers}>
        <View style={styles.Infor}>
          <TouchableOpacity style={styles.Menu}>
            <FontAwesome5 name="bars" size={30} color="white" />
          </TouchableOpacity>
          <TouchableOpacity style={styles.Location}>
            <View style={{flexDirection: 'row'}}>
              <CustomText
                content="Vị trí hiện tại "
                styles={{color: 'white'}}
              />
              <FontAwesome5 name="sort-down" color="white" />
            </View>
            <CustomText
              content={currentAddress || 'Ha Noi,Vietnam'}
              styles={{color: '#F4F4FE'}}
            />
          </TouchableOpacity>
          <TouchableOpacity style={styles.Notification}>
            <FontAwesome5 name="bell" size={30} color="white" />
            <CustomText styles={styles.Amount_unread_notis} content={'5'} />
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          style={styles.SearchBar}
          onPress={() => navigation.navigate('Search', {name: 'Tìm kiếm'})}>
          <CustomText content={'Tìm kiếm ...'} styles={{color: 'gray'}} />
        </TouchableOpacity>
      </View>

      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        style={styles.ContentBar}>
        {ContentArr.map((cnt, index) => (
          <Tag
            key={index}
            content={cnt.content}
            color={cnt.color}
            logo={cnt.logo}
          />
        ))}
      </ScrollView>

      <View style={styles.contentTextWrapper}>
        <CustomText content="Sự kiện nổi bật" styles={styles.contentText} />
        <TouchableOpacity style={styles.more}>
          <CustomText content="Xem tất cả >" styles={styles.moreText} />
        </TouchableOpacity>
      </View>
      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        style={{
          marginTop: 4,
          marginBottom: 20,
        }}>
        {Highlights.map((e, index) => (
          <SingleEvent
            key={index}
            day={e.day}
            month={e.month}
            imgsrc={e.img}
            name={e.name}
            note={e.note}
            location={e.location}
            navigation={navigation}
          />
        ))}
      </ScrollView>

      <View style={styles.contentTextWrapper}>
        <CustomText content="Sự kiện sắp diễn ra" styles={styles.contentText} />
        <TouchableOpacity style={styles.more}>
          <CustomText content="Xem tất cả >" styles={styles.moreText} />
        </TouchableOpacity>
      </View>

      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        style={{
          marginTop: 4,
          marginBottom: 4,
        }}>
        {evenInfor.map((e, index) => (
          <SingleEvent
            key={index}
            day={e.day}
            month={e.month}
            imgsrc={e.img}
            name={e.name}
            note={e.note}
            location={e.location}
            navigation={navigation}
          />
        ))}
      </ScrollView>
      <View style={styles.Footer}></View>
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  Headers: {
    height: 150,
    marginBottom: 30,
    backgroundColor: '#4A43EC',
    borderBottomLeftRadius: 33,
    borderBottomRightRadius: 33,
    width: '100%',
    alignItems: 'center',
  },
  SearchBar: {
    height: 40,
    width: '80%',
    marginTop: 20,
    backgroundColor: 'white',
    justifyContent: 'center',
    paddingLeft: 10,
    borderRadius: 20,
  },
  Infor: {
    justifyContent: 'space-between',
    width: '85%',
    height: 36,
    marginTop: 20,
    flexDirection: 'row',
  },
  Menu: {
    width: 30,
    height: 36,
  },
  Location: {
    // width: 130,
    height: 36,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Notification: {
    width: 36,
    height: 36,
    position: 'relative',
  },
  Amount_unread_notis: {
    position: 'absolute',
    color: '#4A43EC',
    backgroundColor: 'white',
    borderRadius: 100,
    width: 20,
    height: 20,
    fontSize: 15,
    textAlign: 'center',
    right: 5,
    top: -4,
    fontWeight: 'bold',
  },

  SearchInput: {
    marginLeft: 5,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 0,
    paddingBottom: 0,
    flex: 5,
    fontSize: 20,
    opacity: 0.3,
    borderLeftColor: '#7974E7',
    borderLeftWidth: 3,
  },
  SearchInputFocus: {
    marginLeft: 5,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 0,
    paddingBottom: 0,
    flex: 5,
    fontSize: 20,
    color: 'white',
    borderLeftColor: '#7974E7',
    borderLeftWidth: 3,
  },

  FilterWrapper: {
    width: 74,
    height: 32.14,
    backgroundColor: '#5D56F3',
    borderRadius: 50,
    paddingTop: 8,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  ContentBar: {
    top: 130,
    width: '100%',
    position: 'absolute',
    flexDirection: 'row',
  },
  contentTextWrapper: {
    height: 34,
    width: '95%',
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  contentText: {
    width: 200,
    fontSize: 18,
    fontWeight: 'bold',
  },
  // more: {
  //   width: 60,
  // },
  moreText: {
    fontSize: 14,
    fontWeight: '400',
    color: '#747688',
    width: 100,
  },
  Title1: {
    fontWeight: '700',
    fontSize: 18,
    fontStyle: 'normal',
    color: '#120D26',
    alignItems: 'flex-start',
  },
  Footer: {
    height: 20,
  },
});
export default Home;
