import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  TextInput,
  Keyboard,
} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import placesApi from '../../api/placesApi';
import Loading from '../../components/Loading/Loading';
import RBSheet from 'react-native-raw-bottom-sheet';
import {useRef} from 'react';
import axios from 'axios';
import Geolocation from '@react-native-community/geolocation';

function Search({navigation}) {
  const [keyWord, setKeyword] = useState(null);
  const [searchResult, setSearchResult] = useState([]);
  const [fetChing, setFeting] = useState(false);
  const refRBSheet = useRef();
  const [currentPosition, setCurrentPosition] = useState([]);

  const handleSearch = async () => {
    console.log('Search begining');
    setFeting(true);
    try {
      const res = await placesApi.searchPlace(keyWord.toLowerCase());
      setSearchResult(res.data.data);
      setFeting(false);
      Keyboard.dismiss();
      console.log('Search end');
    } catch (err) {
      console.log(err);
      console.log('Search end');
    }
  };

  const searchByRadius = async () => {
    refRBSheet.current.close();
    setFeting(true);
    try {
      const res = await axios.get(
        `https://api.geoapify.com/v2/places?categories=entertainment&filter=circle:${currentPosition[1]},${currentPosition[0]},10000&bias=proximity:${currentPosition[1]},${currentPosition[0]}&limit=20&apiKey=becfd172565140f3b633ab183a6f24c7`,
      );
      const temp = res.data.features;
      const result = temp
        .filter(e => e.properties.name !== undefined)
        .map(item => {
          const props = item.properties;
          return {
            name: props?.name,
            addressLine1: props?.address_line1,
            addressLine2: props?.address_line2,
            coordinates: item?.geometry.coordinates,
            tickets: [],
          };
        });
      setSearchResult(result);
    } catch (err) {
      console.log(err);
    } finally {
      setFeting(false);
    }
  };

  const gotoDetail = item => {
    if (item.title) {
      navigation.navigate('GuideAreaDetail', {
        city: item,
      });
    } else {
      navigation.navigate('EventDetail', {eventInfo: item});
    }
  };

  useEffect(() => {
    Geolocation.getCurrentPosition(info =>
      setCurrentPosition([info.coords.latitude, info.coords.longitude]),
    );
  }, []);

  useEffect(() => {
    const timerId = setTimeout(() => {
      keyWord && handleSearch();
    }, 500);

    return () => clearTimeout(timerId);
  }, [keyWord]);

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={styles.searchBar}>
          <FontAwesome5 name="search" size={20} style={{marginLeft: 10}} />
          <TextInput
            autoFocus={true}
            style={{height: 40}}
            placeholder="Bạn sắp đến đâu?"
            onChangeText={text => setKeyword(text)}
          />
        </View>
        <TouchableOpacity onPress={() => refRBSheet.current.open()}>
          <FontAwesome5 name="filter" size={25} color="#4A43EC" />
        </TouchableOpacity>
      </View>

      <RBSheet
        animationType="slide"
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={false}
        height={600}
        customStyles={{
          wrapper: {
            backgroundColor: 'transparent',
          },
          draggableIcon: {
            backgroundColor: '#000',
          },
        }}>
        <View
          style={{
            width: '100%',
            alignItems: 'center',
            flex: 1,
            backgroundColor: '#f3f1f1',
            paddingTop: 10,
          }}>
          <TouchableOpacity
            onPress={() => searchByRadius()}
            style={{
              backgroundColor: '#4A43EC',
              padding: 10,
              borderRadius: 10,
              alignItems: 'center',
              width: 250,
            }}>
            <Text style={{color: 'white'}}>Tìm kiếm xung quanh</Text>
          </TouchableOpacity>
        </View>
      </RBSheet>

      <ScrollView style={{paddingHorizontal: 20}}>
        {fetChing === true ? (
          <Loading color={'black'} />
        ) : (
          searchResult?.map((item, index) => {
            return (
              <TouchableOpacity
                onPress={() => gotoDetail(item)}
                style={{
                  paddingVertical: 10,
                  borderBottomWidth: 1,
                  borderBottomColor: '#d7d7c1',
                }}
                key={Math.random() + Math.random()}>
                <Text style={{fontSize: 17, color: 'black'}}>
                  {item?.title || item?.name}
                </Text>
              </TouchableOpacity>
            );
          })
        )}
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  searchBar: {
    height: 45,
    borderColor: '#6f707d',
    borderWidth: 2,
    borderRadius: 20,
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: 20,
    marginHorizontal: 20,
    width: '80%',
  },
});

export default Search;
