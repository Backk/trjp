import React, {useEffect, useState} from 'react';
import MapboxGL, {Logger} from '@rnmapbox/maps';
import {View} from 'react-native';
import placesApi from '../../api/placesApi';
MapboxGL.setWellKnownTileServer('Mapbox');
MapboxGL.setAccessToken(
  'sk.eyJ1IjoicHJvMDY1NCIsImEiOiJjbDljdmRzMzEwMHkyM3RteTNhMGs0NDV0In0.3W9OxwdMHjhyZXi0rGIuTQ',
);

// edit logging messages
Logger.setLogCallback(log => {
  const {message} = log;

  // expected warnings - see https://github.com/mapbox/mapbox-gl-native/issues/15341#issuecomment-522889062
  if (
    message.match('Request failed due to a permanent error: Canceled') ||
    message.match('Request failed due to a permanent error: Socket Closed')
  ) {
    return true;
  }
  return false;
});

function Map({route, navigation}) {
  const {cityId, cors} = route.params;
  const [places, setPlaces] = useState([]);

  const getPlaces = async () => {
    try {
      const res = await placesApi.getPlaces({cityId, type: 'entertainment'});
      setPlaces(res.data.data);
    } catch (err) {
      console.log(err);
    }
  };

  const goToDetail = eventInfo => {
    navigation.navigate('EventDetail', {eventInfo});
  };

  useEffect(() => {
    getPlaces();
  }, []);
  // console.log(places);
  return (
    <View style={{flex: 1}}>
      <MapboxGL.MapView
        style={{flex: 1}}
        zoomEnabled={true}
        userTrackingMode={true}>
        <MapboxGL.Camera
          zoomLevel={10}
          centerCoordinate={cors}
          followZoomLevel={10}
        />
        {places &&
          places.map((item, index) => {
            return (
              <MapboxGL.PointAnnotation
                key={item._id}
                id={item._id}
                coordinate={item.coordinates}
                title={item.name}
                // onSelected={() => goToDetail(item)}
              >
                {/* <Text
                  style={{color: '#4A43EC', height: 60, fontWeight: 'bold'}}>
                  <Image
                    style={{
                      width: 40,
                      height: 30,
                    }}
                    source={icons.MapPointIcon}
                  />
                  {item.name}
                </Text> */}

                <MapboxGL.Callout title={item.name} />
              </MapboxGL.PointAnnotation>
            );
          })}
      </MapboxGL.MapView>
    </View>
  );
}

export default Map;
